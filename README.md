# Overview

Simple Promises/A+ conformable Promise library

# Install

```shell
npm install git+ssh://git@bitbucket.org/workslon/parole.git --save
```

# Usage

```javascript
var Parole = require('parole');

var parole = new Parole(function (resolve, reject) {
  setTimeout(function () {
    resolve(123);
  }, 1000);
});

parole
  .then(fucntion (result) {
    console.log(result);
  }, function (error) {
    console.error(error);
  });
```