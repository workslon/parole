(function (name, root, factory) {
  if (typeof module !== 'undefined' && module.exports) module.exports = factory();
  else if (typeof define === 'function' && define.amd) define(factory);
  else root[name] = factory();
}('Parole', this, function () {
  var State = {
    PENDING: 0,
    FULFILLED: 1,
    REJECTED: 2
  };

  function Parole(fn) {
    this.state = State.PENDING;
    this.queue = [];

    if (typeof fn === 'function') {
      fn(fulfill.bind(this), reject.bind(this));
    }
  }

  Parole.prototype.then = function (onFulfilled, onRejected) {
    var promise = new Parole();

    this.queue.push({
      fulfill : typeof onFulfilled == 'function' && onFulfilled,
      reject : typeof onRejected == 'function' && onRejected,
      promise: promise
    });

    run.call(this);

    return promise;
  }

  function fulfill(value) {
    resolve(this, value);
  }

  function reject(reason) {
    transition.call(this, State.REJECTED, reason);
  }

  function run() {
    if (this.state === State.PENDING) return;

    setTimeout((function () {
      while (this.queue.length) {
        var obj = this.queue.shift();
        var value;

        try {
          value = (this.state == State.FULFILLED ?
            (obj.fulfill || function(x) {return x;}) :
            (obj.reject || function(x) {throw x;}))(this.value);
        } catch (error) {
          transition.call(obj.promise, State.REJECTED, error);
          continue;
        }

        resolve(obj.promise, value);
      }
    }).bind(this), 0);
  }

  function resolve(promise, x) {
    if (promise === x) {
      transition.call(promise, State.REJECTED, new TypeError());
    } else if (x && x.constructor === Promise) {
      if (x.state === State.PENDING) {
        x.then(fulfill.bind(promise), reject.bind(promise));
      } else {
        transition.call(promise, x.state, x.value);
      }
    } else if ((typeof x == 'object' || typeof x === 'function') && x != null) {
      var called = false;

      try {
        if (typeof x.then === 'function') {
          x.then.call(x, function(y) {
            called || resolve(promise, y);
            called = true;
          }, function(r) {
            called || transition.call(promise, State.REJECTED, r);
            called = true;
          });
        } else {
          transition.call(promise, State.FULFILLED, x);
        }
      } catch (error) {
        called || transition.call(promise, State.REJECTED, error);
      }
    } else {
      transition.call(promise, State.FULFILLED, x);
    }
  }

  function transition(state, value) {
    if (this.state == state ||
        this.state !== State.PENDING ||
        (state != State.FULFILLED &&
            state != State.REJECTED) ||
        arguments.length < 2) {
      return false;
    }

    this.state = state;
    this.value = value;
    run.call(this);
  }

  return Parole;
}));